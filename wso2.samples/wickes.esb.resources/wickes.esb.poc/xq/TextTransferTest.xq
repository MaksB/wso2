
declare variable $testBody as document-node() external;

declare function local:transform($body as document-node())
      as element() {
	
	<root xmlns="http://ws.apache.org/commons/ns/payload">{
	  for $line in tokenize($body,'\n')
	  return if(not(normalize-space($line) eq "")) then <line>{ 
	  	 for $element at $index in tokenize($line,',')
	  	 	 return if(not(normalize-space($element) eq "")) then
			  	 		if($index eq 1) then <city> {normalize-space($element)} </city>
			  	 		else if($index eq 2 ) then <street> {normalize-space($element) }</street>
						else if ($index eq 3) then <detailAddress> {normalize-space($element)} </detailAddress>
						else <somethingElse> {normalize-space($element)}</somethingElse>
					
					else ()
		  }</line> 
		  else () 	    	   	   
	}</root>
   
};


local:transform($testBody) 
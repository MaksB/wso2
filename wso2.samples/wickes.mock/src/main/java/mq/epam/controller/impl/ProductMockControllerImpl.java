package mq.epam.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mq.epam.controller.ProductMockController;
import mq.epam.jsm.sender.MessageSender;
import mq.epam.model.Product;

@RestController
public class ProductMockControllerImpl implements ProductMockController {

	@Autowired
	private MessageSender messageSender;

	@Autowired
	private Product mocProduct;

	@RequestMapping(value = "/sendProduct", method = RequestMethod.POST)
	@Override
	public Product sendProduct(@RequestParam String name, @RequestParam Integer price, @RequestParam Boolean available,
			@RequestParam(required = false, defaultValue = "false") Boolean toXml) throws Exception {
		Product product = new Product();
		product.setName(name);
		product.setPrice(price);
		product.setAvailable(available);
		messageSender.sendMessage(product, toXml);
		return product;
	}

	@RequestMapping(value = "/getProduct", method = RequestMethod.GET)
	@Override
	public Product getProduct(@RequestParam(required = false, defaultValue = "false") Boolean toXml) {
		messageSender.sendMessage(mocProduct, toXml);
		return mocProduct;
	}
	
	@RequestMapping(value = "/sendMessage", method = RequestMethod.POST)
	@Override
	public String sendText(@RequestParam String text, @RequestParam(required = false, defaultValue = "false") Boolean toXml) {
		messageSender.sendMessage(text, toXml);
		return text;
	}

}

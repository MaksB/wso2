package mq.epam.controller;

import mq.epam.model.Product;

public interface ProductMockController {
	
	public Product sendProduct(String name, Integer price, Boolean enamble, Boolean toXml) throws Exception;

	public Product getProduct(Boolean toXml);
	
	public String sendText(String text, Boolean toXml);
}

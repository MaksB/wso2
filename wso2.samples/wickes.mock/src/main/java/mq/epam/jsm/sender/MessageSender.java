package mq.epam.jsm.sender;

import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

@Component
public class MessageSender {

	private JmsTemplate jmsTemplate;
	
	@Autowired
	private ActiveMQConnectionFactory connectionFactory;
	
	@Value("${queueName}")
	private String queueName;
	
	@PostConstruct
	private void init(){
		jmsTemplate = new JmsTemplate(connectionFactory);
		jmsTemplate.setDefaultDestinationName(queueName);
	}
	
	public <E> void sendMessage(E object, Boolean toXml){
		ObjectMapper objectMapper;
		
		if(toXml){
			objectMapper = new XmlMapper();
		}else{
			objectMapper = new ObjectMapper();
		}
		String message = convertObject(object, objectMapper);
		
		 jmsTemplate.send(new MessageCreator(){  
		        @Override  
		        public Message createMessage(Session session) throws JMSException {  
		            return session.createTextMessage(message);  
		        }
		    });  
	}
	
	private <E> String convertObject(E message,ObjectMapper objectMapper) {
		String result = null;
		try {
			result = objectMapper.writeValueAsString(message);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}
}

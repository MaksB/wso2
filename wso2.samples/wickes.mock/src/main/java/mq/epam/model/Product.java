package mq.epam.model;

public class Product {

	private String name;
	private Integer price;
	private Boolean available;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Boolean getAvailable() {
		return available;
	}
	public void setAvailable(Boolean enable) {
		this.available = enable;
	}
	
	@Override
	public String toString() {
		return "Product [name=" + name + ", price=" + price + ", available=" + available + "]";
	}
	
}

package stok.wso2.task;

import java.util.Date;

import org.apache.axiom.util.UIDGenerator;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.synapse.ManagedLifecycle;
import org.apache.synapse.core.SynapseEnvironment;
import org.apache.synapse.mediators.MediatorFaultHandler;
import org.apache.synapse.mediators.base.SequenceMediator;
import org.apache.synapse.startup.Task;
import org.apache.synapse.MessageContext;

public class ScheduledSequenceTask implements Task, ManagedLifecycle {

	private String sequenceName;
	private SynapseEnvironment synapseEnvironment;

	public void execute() {
		System.out.println("Run task " + new Date());

		if (sequenceName == null) {
			return;
		}

		SequenceMediator seq = (SequenceMediator) synapseEnvironment
				.getSynapseConfiguration().getSequence(sequenceName);
		MessageContext mc = synapseEnvironment.createMessageContext();
		mc.setMessageID(UIDGenerator.generateURNString());
		mc.pushFaultHandler(new MediatorFaultHandler(mc.getFaultSequence()));
		if (seq != null) {

			mc.pushFaultHandler(new MediatorFaultHandler(mc.getFaultSequence()));
			synapseEnvironment.injectAsync(mc, seq);
		}

	}

	public String getSequenceName() {
		return sequenceName;
	}

	public void setSequenceName(String sequenceName) {
		this.sequenceName = sequenceName;
	}

	public void destroy() {
		
	}

	public void init(SynapseEnvironment se) {
		synapseEnvironment = se;
		
	}

}

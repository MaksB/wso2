package com.wickes.poc.wso2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class FileSplitter {
	private long totalFileSize;
	private long timeOut;
	private String totalFileName;
	private StringBuilder splittedFileUris = new StringBuilder() ;
	private String largeFileDir;

	public void execute() throws Exception {
		Boolean statusUploadFile;
		String fileNameWithoutExtension = totalFileName.split("\\.")[0];
		String fileUri = largeFileDir + "/" + totalFileName;
		System.out.println("Total file size is : " + totalFileSize
				+ "; totalFileName : " + totalFileName + "; largeFileDir : "
				+ largeFileDir + "; fileUri : " + fileUri);
		File file = new File(fileUri);
		
		if (file.exists()) {
			file.delete();
		}
		
		statusUploadFile = waitForFileTotallyUploaded(file, timeOut);
		
		if(statusUploadFile){
			splitHugeFile(file, fileNameWithoutExtension);
		}
	}

	private void splitHugeFile(File file, String fileNameWithoutExtension) {

		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			
			FileWriter out = null;

			BufferedWriter brOut = null;
			
			String thisLine = null;
			int lineCounter = 0;
			int fileCounter = 0;

			StringBuilder splittedTextFileBody = new StringBuilder();
			File splittedDir = new File(largeFileDir + "/" + fileNameWithoutExtension);
			splittedDir.mkdirs();
			
			while ((thisLine = br.readLine()) != null) {
				lineCounter++;

				splittedTextFileBody.append(thisLine + System.lineSeparator());
				if (lineCounter > 1000) {

					File splittedFile = new File(largeFileDir
							+ "/" + fileNameWithoutExtension + "/" + fileNameWithoutExtension + "splittedFile" + fileCounter
							+ ".tmp");

				
					out = new FileWriter(splittedFile);

					brOut = new BufferedWriter(out);
					brOut.write(splittedTextFileBody.toString());
					brOut.flush();
					brOut.close();
					splittedTextFileBody.setLength(0);
					splittedTextFileBody.trimToSize(); 
					splittedFileUris.append(splittedFile.getAbsolutePath() + ",");
					lineCounter = 0;
					fileCounter++;
				}
			}

		
		
			if(out != null) out.close();
			if(brOut != null) brOut.close();

			
			if(splittedTextFileBody.length() > 1){
				File splittedFile = new File(largeFileDir
						+ "/" + fileNameWithoutExtension + "/"+ fileNameWithoutExtension + "splittedFile" + fileCounter
						+ ".tmp");
				splittedFile.createNewFile();
				out = new FileWriter(splittedFile);
				brOut = new BufferedWriter(out);
				brOut.write(splittedTextFileBody.toString());
				brOut.flush();
				brOut.close();
				splittedFileUris.append(splittedFile.getAbsolutePath() + ",");
			} 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private Boolean waitForFileTotallyUploaded(File fileUri, long chekTimeOut) {
		long endTimeMillis = System.currentTimeMillis() + chekTimeOut;
		while (fileUri.length() < totalFileSize) {
			try {
				if(System.currentTimeMillis() > endTimeMillis){
					System.out.println("TimeOut");
					splittedFileUris.append("Error: Time out exception");
					return false;
				}
				System.out.println("file.length() = "
						+ fileUri.length() + " totalFileSize = "
						+ totalFileSize + " and sleeping . . . .");
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public String getTotalFileName() {
		return totalFileName;
	}

	public void setTotalFileName(String totalFileName) {
		this.totalFileName = totalFileName;
	}

	public String getSplittedFileUris() {
		return splittedFileUris.toString();
	}

	public void setSplittedFileUris(String splittedFileUris) {
		this.splittedFileUris = new StringBuilder(splittedFileUris);
	}

	public String getLargeFileDir() {
		return largeFileDir;
	}

	public void setLargeFileDir(String largeFileDir) {
		this.largeFileDir = largeFileDir;
	}

	public long getTotalFileSize() {
		return totalFileSize;
	}
	
	public void setTotalFileSize(long totalFileSize) {
		this.totalFileSize = totalFileSize;
	}

	public long getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(long timeOut) {
		this.timeOut = timeOut;
	}

}

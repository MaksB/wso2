package com.wickes.poc.camel.stores;

import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.LinkedList;
import java.util.List;

@Service
public class StoresJMSRouteBuilder extends SpringRouteBuilder {
	private List<String> stores = new LinkedList<>();
	
	@Override
	public void configure() throws Exception {
		from("activemq:queue:Orders")
				.log("INCOMING JMS MESSAGE...")
				.process(exchange -> stores.add(exchange.getIn().getBody(String.class)));
		
		
		from("timer://storesCombiner?delay=20000&period=20000")
				.process(exchange -> {
					exchange.getOut().setBody(StringUtils.collectionToDelimitedString(stores, " | "));
					exchange.getOut().setHeader("CamelFileName", "Stores-" + System.currentTimeMillis() + ".txt");
					stores.clear();
				})
				.choice()
				.when(body().isNotEqualTo(""))
				.log("CREATING FILE...")
				.to("sftp://tester:password@localhost/in?tempFileName=${file:name.noext}.tmp");
	}
}

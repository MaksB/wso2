package com.wickes.poc.camel;

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.camel.component.ActiveMQConfiguration;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.activemq.spring.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.apache.cxf.feature.Feature;
import org.apache.cxf.feature.LoggingFeature;
import org.apache.cxf.transport.common.gzip.GZIPFeature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import java.util.ArrayList;
import java.util.List;

@Configuration
@ComponentScan(basePackages = { "com.wickes.poc.camel" })
@ImportResource("classpath:META-INF/cxf/cxf.xml")
public class CamelContextConfiguration extends CamelConfiguration {

    protected void setupCamelContext(CamelContext camelContext) throws Exception {
        camelContext.setStreamCaching(true);
        super.setupCamelContext(camelContext);
    }

    @Bean(name = "features")
    public List<Feature> features() {
        final List<Feature> result = new ArrayList<>();
        result.add(new LoggingFeature());
        result.add(new GZIPFeature());

        return result;
    }

    @Bean
    public ActiveMQConnectionFactory jmsConnectionFactory() {
        ActiveMQConnectionFactory activeMQConnectionFactory = new ActiveMQConnectionFactory();
        activeMQConnectionFactory.setBrokerURL("tcp://EPUALVIW0605:61616");
        return activeMQConnectionFactory;
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    public PooledConnectionFactory pooledConnectionFactory() {
        PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory();
        pooledConnectionFactory.setMaxConnections(8);
        pooledConnectionFactory.setConnectionFactory(jmsConnectionFactory());
        return pooledConnectionFactory;
    }

    @Bean
    public ActiveMQConfiguration activeMQConfiguration() {
        ActiveMQConfiguration activeMQConfiguration = new ActiveMQConfiguration();
        activeMQConfiguration.setConnectionFactory(pooledConnectionFactory());
        activeMQConfiguration.setConcurrentConsumers(10);
        return activeMQConfiguration;
    }

    @Bean
    public ActiveMQComponent activemq() {
        return new ActiveMQComponent(activeMQConfiguration());
    }
}

package com.wickes.poc.camel.stores;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Service;

@Service
public class StoresFTPRouteBuilder extends SpringRouteBuilder {

    // we use a delay of 10 sec (eg. once pr. hour we poll the SFTP server)
    long delay = 10000;

    @Override
    public void configure() throws Exception {
        from("sftp://tester:password@localhost/out?binary=true&move=arch&moveFailed=error&include=.*.txt|.*.zip&initialDelay=" + delay + "&delay=" + delay)
                .log(LoggingLevel.INFO, "File name is [${header." + Exchange.FILE_NAME_ONLY + "}]")
                .to("sftp://tester:password@localhost/in?tempFileName=${file:name.noext}.tmp");
    }
}
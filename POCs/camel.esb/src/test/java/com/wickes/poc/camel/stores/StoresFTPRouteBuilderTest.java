package com.wickes.poc.camel.stores;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.spring.javaconfig.SingleRouteCamelConfiguration;
import org.apache.camel.test.spring.CamelSpringDelegatingTestContextLoader;
import org.apache.camel.test.spring.CamelSpringJUnit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;

@RunWith(CamelSpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {StoresFTPRouteBuilderTest.ContextConfig.class}, loader = CamelSpringDelegatingTestContextLoader.class)
public class StoresFTPRouteBuilderTest extends AbstractJUnit4SpringContextTests {
	@Autowired
	private CamelContext camelContext;
	
	@Autowired
	private ProducerTemplate template;
	
	@EndpointInject(uri = "mock:sftp")
	private MockEndpoint sftpFromEndpoint;
	
	@EndpointInject(uri = "mock:sftp")
	private MockEndpoint sftpToEndpoint;
	
	
	@Test
	public void testMockAllEndpoints() throws InterruptedException {
		sftpToEndpoint.expectedBodiesReceived("Hello");
		
		template.sendBody(sftpFromEndpoint, "Hello");
		MockEndpoint.assertIsSatisfied(camelContext);
	}
	
	@Configuration
	public static class ContextConfig extends SingleRouteCamelConfiguration {
		@Bean
		public RouteBuilder route() {
			return new StoresFTPRouteBuilder();
		}
	}
}

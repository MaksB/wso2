declare namespace xf = "http://www.wickes.com/wso2.esb.poc/ConvertFTPFile/";
declare variable $textBody as xs:string external;
declare variable $fileProcessedDaily as xs:boolean external;


declare function xf:convertToXml($testBody)
{
<addStore>{
for $row in tokenize($testBody, '\n')
where string-length($row) > 1
return 
let $x := tokenize($row, "\|")
return 
	<store>
	<org_name_full>{normalize-space($x[1])}</org_name_full> 
	<bas_addr_1>{normalize-space($x[2])}</bas_addr_1>
	<bas_addr_2>{normalize-space($x[3])}</bas_addr_2>
	<bas_city>{normalize-space($x[4])}</bas_city>
	<bas_state>{normalize-space($x[5])}</bas_state>
	<bas_zip>{normalize-space($x[6])}</bas_zip>
	<cntry_code>{normalize-space($x[7])}</cntry_code>
	<bas_area>{normalize-space($x[8])}</bas_area>
	<bas_area_fax>{normalize-space($x[9])}</bas_area_fax>
	<org_lvl_parent_num_4>{normalize-space($x[10])}</org_lvl_parent_num_4>
	<org_lvl_parent_num_3>{normalize-space($x[11])}</org_lvl_parent_num_3>
	<org_lvl_parent_num_2>{normalize-space($x[12])}</org_lvl_parent_num_2>
	<org_manager_name>{normalize-space($x[13])}</org_manager_name>
	<org_date_opened>{normalize-space($x[14])}</org_date_opened>
	<org_date_closed>{normalize-space($x[15])}</org_date_closed>
	<org_lvl_number_s>{normalize-space($x[16])}</org_lvl_number_s>
	<daily_processed>{$fileProcessedDaily}</daily_processed>
	</store>
}</addStore>
};

xf:convertToXml($textBody)
 
 
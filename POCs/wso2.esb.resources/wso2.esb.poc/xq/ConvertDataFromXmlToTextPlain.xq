declare namespace s = "http://www.wickes.com/filetransport";
declare variable $xmlBody as document-node() external;

declare function local:convertToText($body as document-node()) {
     <text xmlns="http://ws.apache.org/commons/ns/payload">{
     	for $storeElement at $index in $body//s:store
	    return concat($storeElement/s:org_name_full, " | ", $storeElement/s:bas_addr_1, " | ", $storeElement/s:bas_addr_2, " | ", 
	    			$storeElement/s:bas_city, " | ", $storeElement/s:bas_state, " | ", $storeElement/s:bas_zip, " | ", 
	    			$storeElement/s:cntry_code, " | ", $storeElement/s:bas_area, " | ", $storeElement/s:bas_area_fax, " | ", 
	    			$storeElement/s:org_lvl_parent_num_4, " | ", $storeElement/s:org_lvl_parent_num_3, " | ", $storeElement/s:org_lvl_parent_num_2, " | ", 
	    			$storeElement/s:org_manager_name, " | ", $storeElement/s:org_date_opened, " | ", $storeElement/s:org_date_closed, " | ",
	    			$storeElement/s:org_lvl_number_s, "	&#10;")
   	}</text>
};

local:convertToText($xmlBody)
 
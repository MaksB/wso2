
declare variable $textBody as document-node() external;

declare function local:transform($body as document-node())
      as element() {
	
	<root xmlns="http://ws.apache.org/commons/ns/payload">{
	  for $line in tokenize($body,'\n')
	  return if(not(normalize-space($line) eq "")) then <line>{ 
	  	 for $element at $index in tokenize($line,',')
	  	 	 return if(not(normalize-space($element) eq "")) then
			  	 		if($index eq 1) then <org_name_full> {normalize-space($element)} </org_name_full>
			  	 		else if($index eq 2 ) then <bas_addr_1> {normalize-space($element) }</bas_addr_1>
						else if ($index eq 3) then <bas_addr_2> {normalize-space($element)} </bas_addr_2>
						else if ($index eq 4) then <bas_city> {normalize-space($element)} </bas_city>
						else if ($index eq 5) then <bas_state> {normalize-space($element)} </bas_state>
						else if ($index eq 6) then <bas_zip> {normalize-space($element)} </bas_zip>
						else if ($index eq 7) then <cntry_code> {normalize-space($element)} </cntry_code>
						else if ($index eq 8) then <bas_area> {normalize-space($element)} </bas_area>
						else if ($index eq 9) then <bas_area_fax> {normalize-space($element)} </bas_area_fax>
						else if ($index eq 10) then <org_lvl_parent_num_4> {normalize-space($element)} </org_lvl_parent_num_4>
						else if ($index eq 11) then <org_lvl_parent_num_3> {normalize-space($element)} </org_lvl_parent_num_3>
						else if ($index eq 12) then <org_lvl_parent_num_2> {normalize-space($element)} </org_lvl_parent_num_2>
						else if ($index eq 13) then <org_manager_name> {normalize-space($element)} </org_manager_name>
						else if ($index eq 14) then <org_date_opened> {normalize-space($element)} </org_date_opened>
						else if ($index eq 15) then <org_date_closed> {normalize-space($element)} </org_date_closed>
						else <org_lvl_number_s> {normalize-space($element)}</org_lvl_number_s>
					
					else ()
		  }</line> 
		  else () 	    	   	   
	}</root>
   
};


local:transform($textBody) 
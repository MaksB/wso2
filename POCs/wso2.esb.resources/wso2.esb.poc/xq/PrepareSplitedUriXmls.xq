declare variable $commaSeparatedFileUris as xs:string external;

declare function local:convertToXmlStructure($body as xs:string) {
     <uris>{
     	for $uri at $index in tokenize($commaSeparatedFileUris,',')
	    return 
	    	if(not(normalize-space($uri) eq "")) then
	    		<uri>{ $uri }</uri>
	    	else ()
   	}</uris>
};

local:convertToXmlStructure($commaSeparatedFileUris)
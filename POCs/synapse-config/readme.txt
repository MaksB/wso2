Project synapse-config contains synapse configuration XSDs from 'http://ws.apache.org/ns/synapse'

How to use:
 - Build it with 'mvn -Plocal clean package dependency:copy -e'
        The 'synapse-configuration-4.10.jar' file will be built and copied to the WSO2_HOME/repository/components/plugins folder.
 - Open the Carbon Studio
 - Go to Window -> Preferences -> XML -> XML Catalog
 - Add new Catalog Entry to User Specified Entries with the following parameters:
        Location:    jar:file:/D:/psp/wso2/servers/wso2esb-4.0.3/repository/components/plugins/synapse-configuration-4.10.jar!/xsd/synapse_config.xsd
        Key Type:    Namespace name
        Key:         http://ws.apache.org/ns/synapse
 - Open any WSO2 synapse configuration in the XML editor
 - Now you can use hot keys 'Ctrl + Space' to show the prompt area with the synapse elements and mediators